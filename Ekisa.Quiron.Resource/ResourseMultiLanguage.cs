﻿namespace Ekisa.Quiron
{
    using System.Globalization;
    using System.Threading;
    public class ResourseMultiLanguage
    {
        public void SetSystemLanguage(string Culture)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Culture);
        }
    }
}
