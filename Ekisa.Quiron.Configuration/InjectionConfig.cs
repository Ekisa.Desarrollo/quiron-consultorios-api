﻿using Ekisa.Quiron.Common;
namespace Ekisa.Quiron.Configuration
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Model;
    using Repository;
    using Rule;
    public static class InjectionConfig
    {
        public static IServiceCollection RegisterServices(
            this IServiceCollection service, IConfiguration configuration)
        {
            DataBase(service);
            Rules(service);
            RulesSecurity(service);
            Repository(service);
            RepositorySecurity(service);
            Common(service);

            return service;
        }

        private static void DataBase(this IServiceCollection service)
        {
            service.AddDbContext<QuironContext>();
        }

        private static void RulesSecurity(this IServiceCollection service)
        {
            service.AddScoped<IUserRule, UserRule>();
            service.AddScoped<IAuthorizationRule, AuthorizationRule>();
        }

        private static void Rules(this IServiceCollection service)
        {
            service.AddScoped<IMessage, Message>();
        }

        private static void Repository(this IServiceCollection service)
        {
            service.AddScoped<IInclusion, Inclusion>();
            service.AddScoped<IParameterRepository, ParameterRepository>();
        }

        private static void RepositorySecurity(this IServiceCollection service)
        {
            service.AddScoped<IUserRepository, UserRepository>();
            service.AddScoped<IAuthorizationRepository, AuthorizationRepository>();
        }

        private static void Common(this IServiceCollection service)
        {
            service.AddScoped<IEmail, Email>();
            service.AddScoped<IConfigureEmailRule, ConfigureEmailRule>();
        }
    }
}
