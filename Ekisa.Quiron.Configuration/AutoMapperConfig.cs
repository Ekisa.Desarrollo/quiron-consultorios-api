﻿namespace Ekisa.Quiron.Configuration
{
    using AutoMapper;
    using Dto;
    using Model;
    public static class AutoMapperConfig
    {
        #region Configuration
        public static void Configure()
        {
            Mapper.Initialize(config =>
            {
                MapperSecuriry(config);
            });
        }
        #endregion

        #region Security

        private static void MapperSecuriry(IMapperConfigurationExpression config)
        {
            config.CreateMap<User, UserDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(source => source.UserId))
                .ReverseMap();

            config.CreateMap<Parameters, ParameterDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(source => source.ParameterId))
                .ReverseMap();

        }
        #endregion
    }
}
