﻿namespace Ekisa.Quiron.Common
{
    using Dto;

    public static class Message
    {
        #region Public Methods
        public static MessageDto SetMessage(TypeMessage type, string text, bool flag)
        {
            return new MessageDto
            {
                Type = type,
                Message = text,
                Flag = flag
            };
        }
        #endregion

    }
}
