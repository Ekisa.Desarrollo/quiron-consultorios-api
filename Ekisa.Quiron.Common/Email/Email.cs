﻿namespace Ekisa.Quiron.Common
{
    using System;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Threading.Tasks;
    using Dto;
    public class Email : IEmail
    {
        #region Private Attributes
        #endregion

        #region Constructor
        public Email()
        {
        }

        #endregion
        public async Task<bool> SendEmail(ConfigureEmailDto dto)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient(dto.SenderEmail.ServerSmtp);

                mail.From = new MailAddress(dto.SenderEmail.Email);
                MailReceiver(dto, mail);

                mail.Subject = dto.ReceiverEmail.Affair;
                mail.IsBodyHtml = true;
                mail.Body = dto.ReceiverEmail.BodyHtml;

                MailAttached(dto, mail);

                smtpServer.Port = dto.SenderEmail.Port;
                smtpServer.Credentials = new System.Net.NetworkCredential(dto.SenderEmail.Email, dto.SenderEmail.Password);
                smtpServer.EnableSsl = dto.SenderEmail.EnableSsl;

                await smtpServer.SendMailAsync(mail);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private static void MailAttached(ConfigureEmailDto dto, MailMessage mail)
        {
            if (dto.ReceiverEmail.SendAttached)
            {
                dto.ReceiverEmail.AttachedFiles.ForEach(x =>
                {
                    ContentType contentType = new ContentType
                    { MediaType = MediaTypeNames.Application.Octet, Name = $"{x.NameFile}{x.Extention}" };

                    mail.Attachments.Add(new Attachment(x.File, contentType));
                });
            }
        }

        private static void MailReceiver(ConfigureEmailDto dto, MailMessage mail)
        {
            if (dto.ReceiverEmail.Emails != null)
            {
                foreach (var t in dto.ReceiverEmail.Emails)
                {
                    mail.To.Add(t);
                }
            }
            else
            {
                mail.To.Add(dto.ReceiverEmail.Email);
            }
        }
    }
}
