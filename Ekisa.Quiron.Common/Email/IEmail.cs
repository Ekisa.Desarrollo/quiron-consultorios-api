﻿namespace Ekisa.Quiron.Common
{
    using System.Threading.Tasks;
    using Dto;
    public interface IEmail
    {
        Task<bool> SendEmail(ConfigureEmailDto dto);
    }
}
