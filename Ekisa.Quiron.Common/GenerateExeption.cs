﻿namespace Ekisa.Quiron.Common
{
    using System;
    public static class GenerateCustomExeption
    {
        public static string ExeptionMessage(Exception ex)
        {
            return ex.InnerException != null && ex.InnerException?.Message == "common" ? ex.Message : Resource.Resource.Exception;
        }
    }
}
