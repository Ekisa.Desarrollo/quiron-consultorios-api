﻿namespace Ekisa.Quiron.Model
{
    using System.ComponentModel.DataAnnotations;
    public class Parameters
    {
        [Key]
        public int ParameterId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int? CompanyId { get; set; }
        public bool Active { get; set; }
    }
}
