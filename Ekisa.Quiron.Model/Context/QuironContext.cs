﻿namespace Ekisa.Quiron.Model
{
    using Common;
    using Microsoft.EntityFrameworkCore;
    public class QuironContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Configuration.ConnectionString);
        }

        #region DbSet Security
        public DbSet<User> User { get; set; }
        public DbSet<Parameters> Parameters { get; set; }
        #endregion
    }
}
