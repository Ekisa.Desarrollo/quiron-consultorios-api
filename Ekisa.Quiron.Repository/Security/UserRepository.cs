﻿namespace Ekisa.Quiron.Repository
{
    using AutoMapper;
    using Dto;
    using Model;
    using Rule;
    using System.Collections.Generic;
    using System.Linq;
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        #region Private Attibutes
        private readonly IInclusion _inclusion;
        #endregion
        #region Constructor
        public UserRepository(QuironContext contextApp, IInclusion inclusion) : base(contextApp)
        {
            _inclusion = inclusion;
        }

        #endregion

        #region Public Methods
        public UserDto Create(UserDto dto)
        {
            var data = Mapper.Map<UserDto, User>(dto);
            data.Password = "12345";
            Add(data);
            Save();
            return Mapper.Map<User, UserDto>(data);
        }

        public UserDto Update(UserDto dto)
        {
            var data = FindUserById(dto.Id);
            if (data == null) return null;
            Edit(data, Mapper.Map<UserDto, User>(dto));
            Save();
            return Mapper.Map<User, UserDto>(data);
        }

        public void Delete(int id)
        {
            var data = FindUserById(id);
            Delete(data);
            Save();
        }

        public UserDto GetUserByUserName(string userName)
        {
            var data = Find(x => x.UserName == userName).FirstOrDefault();
            return Mapper.Map<User, UserDto>(data);
        }

        public List<UserDto> GetAllUser()
        {
            List<User> users = GetAll().ToList();
            return Mapper.Map<List<User>, List<UserDto>>(users);
        }

        #endregion

        #region Private Methods
        private User FindUserById(int id)
        {
            var data = Find(x => x.UserId == id).FirstOrDefault();
            return data;
        }
        #endregion
    }
}
