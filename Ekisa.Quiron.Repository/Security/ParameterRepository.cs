﻿namespace Ekisa.Quiron.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Dto;
    using Model;
    using Rule;
    public class ParameterRepository : GenericRepository<Parameters>, IParameterRepository
    {
        #region Constructor

        public ParameterRepository(QuironContext contextApp) : base(contextApp)
        {
        }
        #endregion

        #region Public Methods
        public List<ParameterDto> GetAllParameter(int companyId)
        {
            var parameter = GetAll().ToList();
            return Mapper.Map<List<Parameters>, List<ParameterDto>>(parameter);
        }

        public List<ParameterDto> GetParameterByName(string name)
        {
            var parameter = Find(x => x.Name.Equals(name)).ToList();
            return Mapper.Map<List<Parameters>, List<ParameterDto>>(parameter);
        }
        #endregion
    }
}
