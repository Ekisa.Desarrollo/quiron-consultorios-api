﻿namespace Ekisa.Quiron.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Dto;
    using Model;
    using Rule;
    public class AuthorizationRepository : GenericRepository<User>, IAuthorizationRepository
    {
        #region Private Attributes
        private readonly IInclusion _inclusion;
        #endregion

        #region Constructor
        public AuthorizationRepository(QuironContext contextApp) : base(contextApp)
        {
        }
        #endregion

        //TODO: Implementar autenticacion contra la db
        private readonly List<AuthorizationDto> _authorizationDto = new List<AuthorizationDto>
        {
            new AuthorizationDto { UserId = 1, UserName = "test@test.com", FullName = "test.test"}
        };

        #region Public Methods

        public AuthorizationDto Authenticate(LoginDto dto)
        {
            return _authorizationDto.SingleOrDefault(x => x.UserName == dto.UserName);
        }

        public UserDto SendForgotPassword(string userName)
        {
            throw new System.NotImplementedException();
        }

        public void ForgotPassword(int userId, string newPassword)
        {
            User user = Find(e => e.UserId.Equals(userId)).FirstOrDefault();
            if (user == null) return;
            user.Password = newPassword;
            Save();
        }
        #endregion
    }
}
