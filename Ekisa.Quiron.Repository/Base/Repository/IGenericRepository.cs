﻿namespace Ekisa.Quiron.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    public interface IGenericRepository<T>
        where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(string includeProperties);
        IEnumerable<T> Find(Expression<Func<T, bool>> function);
        IEnumerable<T> Find(Expression<Func<T, bool>> function, string includeProperties);
        T Add(T entity);
        List<T> Add(List<T> entity);
        void Delete(T entity);
        void Delete(List<T> entity);
        void Edit(T entity);
        void Save();

        void StartTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }
}