﻿namespace Ekisa.Quiron.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Storage;
    using Model;

    public class GenericRepository<T> : IGenericRepository<T>, IGenericRepositoryAsync<T>
    where T : class
    {
        #region Properties
        private readonly DbContext _context;
        private IDbContextTransaction _transaction;
        #endregion

        #region Constructor
        protected GenericRepository(QuironContext contextApp)
        {
            _context = contextApp;
        }
        #endregion

        #region Public Methods               

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>().AsEnumerable();
        }

        public Task<List<T>> GetAllAsync()
        {
            return _context.Set<T>().ToListAsync();
        }

        public IEnumerable<T> GetAll(string includeProperties)
        {
            IQueryable<T> petition = _context.Set<T>();
            return IncludeProperties(petition, includeProperties).AsEnumerable();
        }

        public async Task<List<T>> GetAllAsync(string includeProperties)
        {
            IQueryable<T> petition = _context.Set<T>();
            return await IncludeProperties(petition, includeProperties).ToListAsync();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> function)
        {
            return _context.Set<T>().Where(function).AsEnumerable();
        }

        public Task<List<T>> FindAsync(Expression<Func<T, bool>> function)
        {
            return _context.Set<T>().Where(function).ToListAsync();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> function, string includeProperties)
        {
            IQueryable<T> petition = _context.Set<T>().Where(function);
            return IncludeProperties(petition, includeProperties).AsEnumerable();
        }

        public Task<List<T>> FindAsync(Expression<Func<T, bool>> function, string includeProperties)
        {
            IQueryable<T> petition = _context.Set<T>().Where(function);
            return IncludeProperties(petition, includeProperties).ToListAsync();
        }

        public T Add(T entity)
        {
            _context.Set<T>().Add(entity);
            return entity;
        }

        public List<T> Add(List<T> entity)
        {
            _context.Set<T>().AddRange(entity);
            return entity;
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Attach(entity);
            _context.Set<T>().Remove(entity);
        }

        public void Delete(List<T> entity)
        {
            _context.Set<T>().AttachRange(entity);
            _context.Set<T>().RemoveRange(entity);
        }

        public void Edit(T entity)
        {
            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Edit(T entity, T model)
        {
            _context.Entry(entity).CurrentValues.SetValues(model);
            _context.Entry(entity).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void StartTransaction()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _transaction.Commit();
        }

        public void RollbackTransaction()
        {
            _transaction.Rollback();
        }
        #endregion

        #region Private Method
        private IQueryable<T> IncludeProperties(IQueryable<T> petition, string includeProperties)
        {
            return includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).
                Aggregate(petition, (current, includeProperty) => current.Include(includeProperty));
        }
        #endregion
    }
}
