﻿namespace Ekisa.Quiron.Repository
{
    public interface IInclusion
    {
        void AddInclusion(params string[] inclusions);
        void ClearInclusions();
        string GetInclusions();
    }
}
