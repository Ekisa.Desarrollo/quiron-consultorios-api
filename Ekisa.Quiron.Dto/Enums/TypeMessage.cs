﻿namespace Ekisa.Quiron.Dto
{
    public enum TypeMessage
    {
        Success = 0,
        Error = 1,
        Warning = 2,
        Exception = 3,
        Required = 4
    }
}
