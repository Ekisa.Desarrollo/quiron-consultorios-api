﻿namespace Ekisa.Quiron.Dto
{
    public class ParameterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int? CompanyId { get; set; }
        public bool Active { get; set; }
    }
}
