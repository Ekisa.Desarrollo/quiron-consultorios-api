﻿namespace Ekisa.Quiron.Dto
{
    using System;
    public class AuthorizationDto
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}
