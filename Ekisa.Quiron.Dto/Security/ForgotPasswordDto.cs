﻿namespace Ekisa.Quiron.Dto
{
    public class ForgotPasswordDto
    {
        public string _token { get; set; }
        public string NewPassword { get; set; }
    }
}
