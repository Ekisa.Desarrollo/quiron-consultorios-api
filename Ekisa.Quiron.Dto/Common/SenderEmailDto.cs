﻿namespace Ekisa.Quiron.Dto
{
    public class SenderEmailDto
    {
        public string Email { get; set; }
        public string ServerSmtp { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public bool EnableSsl { get; set; }
    }
}
