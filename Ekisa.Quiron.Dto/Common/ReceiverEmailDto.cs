﻿namespace Ekisa.Quiron.Dto
{
    using System.Collections.Generic;
    public class ReceiverEmailDto
    {
        public string Email { get; set; }
        public string[] Emails { get; set; }
        public string Affair { get; set; }
        public string BodyHtml { get; set; }
        public bool SendAttached { get; set; }
        public List<AttachedFile> AttachedFiles { get; set; }
    }
}
