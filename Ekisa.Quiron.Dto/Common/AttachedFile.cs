﻿namespace Ekisa.Quiron.Dto
{
    using System.IO;
    public class AttachedFile
    {
        public Stream File { get; set; }
        public string NameFile { get; set; }
        public string Extention { get; set; }
    }
}
