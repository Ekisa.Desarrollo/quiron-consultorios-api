﻿namespace Ekisa.Quiron.Dto
{
    public class MessageDto
    {
        #region Properties
        public TypeMessage Type { get; set; }
        public string Message { get; set; }
        public bool Flag { get; set; }
        #endregion
    }
}
