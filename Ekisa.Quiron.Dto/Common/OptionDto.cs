﻿namespace Ekisa.Quiron.Dto
{
    public class OptionDto
    {
        #region Properties
        public int Value { get; set; }
        public string Text { get; set; }
        #endregion
    }
}
