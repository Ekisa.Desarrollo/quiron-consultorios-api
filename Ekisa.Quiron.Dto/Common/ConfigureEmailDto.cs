﻿namespace Ekisa.Quiron.Dto
{
    public class ConfigureEmailDto
    {
        public SenderEmailDto SenderEmail { get; set; }
        public ReceiverEmailDto ReceiverEmail { get; set; }
    }
}
