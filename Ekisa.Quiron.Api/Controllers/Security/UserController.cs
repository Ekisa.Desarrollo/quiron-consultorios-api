﻿namespace Ekisa.Quiron.Api.Controllers.Security
{
    using System;
    using Common;
    using Dto;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Resource;
    using Rule;

    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        #region Private Attributes
        private readonly IUserRule _rule;
        private readonly ILogger<UserController> _logger;
        private readonly IMessage _message;
        #endregion

        #region Constructor
        public UserController(ILogger<UserController> logger,
            IMessage message,
            IUserRule rule)
        {
            _logger = logger;
            _message = message;
            _rule = rule;
        }
        #endregion

        #region Public Methods
        [HttpPost("Create")]
        public IActionResult Create([FromBody] UserDto dto)
        {
            try
            {
                dto = _rule.Create(dto);
                return Ok(new { ModelData = dto, _rule.Message });
            }
            catch (Exception ex)
            {
                return GenerateException(ex);
            }
        }

        [HttpPost("Update")]
        public IActionResult UpdateUser([FromBody] UserDto dto)
        {
            try
            {
                var data = _rule.Update(dto);
                return Ok(new { ModelData = data, _rule.Message });
            }
            catch (Exception ex)
            {
                return GenerateException(ex);
            }
        }

        [HttpDelete("Delete")]
        public IActionResult Delete(int id)
        {
            try
            {
                var data = _rule.Delete(id);
                return Ok(new { ModelData = data, _rule.Message });
            }
            catch (Exception ex)
            {
                return GenerateException(ex);
            }
        }

        [HttpGet("GetUserByUserName")]
        public IActionResult GetUserByUserName(string userName)
        {
            try
            {
                var dto = _rule.GetUserByUserName(userName);
                return Ok(new { ModelData = dto, _rule.Message });
            }
            catch (Exception ex)
            {
                return GenerateException(ex);
            }
        }

        [HttpGet("GetAll")]
        public IActionResult GetAllUser()
        {
            try
            {
                var dto = _rule.GetAll();
                return Ok(new { ModelData = dto, _rule.Message });
            }
            catch (Exception ex)
            {
                return GenerateException(ex);
            }
        }
        #endregion

        #region Private Methods
        private IActionResult GenerateException(Exception ex)
        {
            _logger.LogError(ex, Resource.Exception);
            return StatusCode(500, new { Message = _message.Exception(GenerateCustomExeption.ExeptionMessage(ex)) });
        }
        #endregion
    }
}