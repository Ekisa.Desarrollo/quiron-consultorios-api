﻿namespace Ekisa.Quiron.Api.Controllers.Auth
{
    using System;
    using System.Threading.Tasks;
    using Common;
    using Dto;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using Resource;
    using Rule;
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AuthController : Controller
    {
        #region Properties
        private readonly ILogger<AuthController> _logger;
        private readonly IMessage _message;
        private readonly IAuthorizationRule _rule;
        #endregion

        #region Constructor
        public AuthController(ILogger<AuthController> logger,
            IMessage message,
            IAuthorizationRule authorizationRule)
        {
            _logger = logger;
            _message = message;
            _rule = authorizationRule;
        }
        #endregion

        #region Public Methods
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody] LoginDto loginDto)
        {
            try
            {
                var dto = _rule.Authenticate(loginDto);

                return Ok(new { ModelData = dto, _rule.Message });
            }
            catch (Exception ex)
            {
                return GenerateException(ex);
            }
        }
        #endregion

        #region Private Methods
        private IActionResult GenerateException(Exception ex)
        {
            _logger.LogError(ex, Resource.Exception);
            return StatusCode(500, new { Message = _message.Exception(GenerateCustomExeption.ExeptionMessage(ex)) });
        }
        #endregion
    }
}