﻿namespace Ekisa.Quiron.Rule
{
    using Dto;
    using System.Collections.Generic;
    public interface IUserRepository
    {
        UserDto Create(UserDto dto);
        UserDto Update(UserDto dto);
        void Delete(int id);
        UserDto GetUserByUserName(string userName);
        List<UserDto> GetAllUser();
    }
}
