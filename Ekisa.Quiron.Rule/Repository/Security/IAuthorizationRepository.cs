﻿namespace Ekisa.Quiron.Rule
{
    using Dto;
    public interface IAuthorizationRepository
    {
        AuthorizationDto Authenticate(LoginDto dto);

        UserDto SendForgotPassword(string userName);

        void ForgotPassword(int userId, string newPassword);
    }
}
