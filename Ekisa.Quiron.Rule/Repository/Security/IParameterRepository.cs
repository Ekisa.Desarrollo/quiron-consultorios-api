﻿namespace Ekisa.Quiron.Rule
{
    using System.Collections.Generic;
    using Dto;
    public interface IParameterRepository
    {
        List<ParameterDto> GetAllParameter(int companyId);
        List<ParameterDto> GetParameterByName(string name);

    }
}
