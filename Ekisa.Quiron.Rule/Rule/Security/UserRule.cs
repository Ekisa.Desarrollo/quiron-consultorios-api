﻿namespace Ekisa.Quiron.Rule
{
    using Dto;
    using System;
    using System.Collections.Generic;
    public class UserRule: IUserRule
    {
        #region Private Attributes
        private readonly IMessage _message;
        private readonly IUserRepository _repository;
        #endregion

        #region Properties
        public MessageDto Message { get; private set; }
        #endregion

        #region Constructor
        public UserRule(IMessage message, IUserRepository repository)
        {
            _message = message;
            _repository = repository;
        }
        #endregion

        #region Public Methods
        public UserDto Create(UserDto dto)
        {
            try
            {
                if (Validate(dto))
                {
                    dto = _repository.Create(dto);
                    Message = _message.Success();
                }
            }
            catch (Exception ex)
            {
                Message = _message.Exception(ex.Message);
            }

            return dto;
        }

        public UserDto Update(UserDto dto)
        {
            var data = _repository.Update(dto);
            Message = data == null ? _message.Warning(Resource.Resource.UserNotExists) : _message.Success(Resource.Resource.UpdateSuccess);
            return data;
        }

        public bool Delete(int id)
        {
            try
            {
                Message = _message.Success(Resource.Resource.DeleteSuccess);
                _repository.Delete(id);
                return true;
            }
            catch (Exception)
            {
                throw new Exception(Resource.Resource.ErrorDelete, new Exception("common"));
            }
        }

        public UserDto GetUserByUserName(string userName)
        {
            Message = _message.Success();
            return _repository.GetUserByUserName(userName);
        }

        public List<UserDto> GetAll()
        {
            Message = _message.Success();
            return _repository.GetAllUser();
        }

        #endregion

        #region Private Methods
        private bool Validate(UserDto dto)
        {
            if (!string.IsNullOrEmpty(dto.UserName)) return true;
            Message = _message.Warning("");
            return false;

        }
        #endregion
    }
}
