﻿namespace Ekisa.Quiron.Rule
{
    using Dto;
    using Microsoft.Extensions.Configuration;
    using Microsoft.IdentityModel.Tokens;
    using Resource;
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using System.Text;
    using System.Threading.Tasks;
    public class AuthorizationRule : IAuthorizationRule
    {
        #region Private Attributes
        public MessageDto Message { get; private set; }

        private readonly IMessage _message;
        private readonly IConfiguration _configuration;
        private readonly IAuthorizationRepository _authorizationRepository;
        private readonly IConfigureEmailRule _configureEmailRule;
        private readonly IParameterRepository _parameterRepository;
        #endregion

        #region Constructor

        public AuthorizationRule(IMessage message,
            IConfiguration configuration,
            IAuthorizationRepository authorizationRepository,
            IConfigureEmailRule configureEmailRule,
            IParameterRepository parameterRepository)
        {
            _message = message;
            _configuration = configuration;
            _authorizationRepository = authorizationRepository;
            _configureEmailRule = configureEmailRule;
            _parameterRepository = parameterRepository;
        }
        #endregion

        #region Public Methods
        public void ForgotPassword(ForgotPasswordDto dto)
        {
            try
            {
                var claims = ReadToken(dto._token);
                if (claims == null) return;
                var userId = Convert.ToInt32(claims.Claims.Where(x => x.Type.Equals("UserId")).Select(x => x.Value).FirstOrDefault());
                _authorizationRepository.ForgotPassword(userId, dto.NewPassword);
                Message = _message.Success(Resource.MessageResetPassword);
            }
            catch (Exception ex)
            {
                Message = _message.Exception(ex.Message);
            }
        }

        public async Task<bool> SendForgotPassword(string userName)
        {
            try
            {
                var user = _authorizationRepository.SendForgotPassword(userName);
                if (user != null)
                {
                    await _configureEmailRule.SendEmailAsync(ConfigureEmail(userName, user.FullName, user.Id)).ConfigureAwait(false);
                }
                Message = user != null ? _message.Success(Resource.MessageResetPasswordSent) : _message.Warning(Resource.UserNotExists);
                return user != null;
            }
            catch (Exception ex)
            {
                Message = _message.Exception(ex.Message);
            }
            return false;
        }

        public AuthorizationDto Authenticate(LoginDto dto)
        {
            try
            {
                var authenticate = _authorizationRepository.Authenticate(dto);
                if (authenticate == null)
                {
                    Message = _message.Error(Resource.IncorrectLogin);
                    return new AuthorizationDto();
                }

                var expiration = DateTime.UtcNow;

                var claimsdata = new[]
                {
                    new Claim("UserName", authenticate.UserName),
                    new Claim("UserId", authenticate.UserId.ToString()),
                    new Claim("RoleId", authenticate.RoleId.ToString())
                };

                var token = GenerateToken(claimsdata, Convert.ToInt32(_configuration["ApiAuth:ExpirationMinute"]));
                authenticate.Token = token;
                authenticate.Expiration = expiration;

                Message = _message.Success();
                return authenticate;
            }
            catch (Exception ex)
            {
                Message = _message.Exception(ex.Message);
                throw new Exception(ex.Message, new Exception("common"));
            }
        }
        #endregion

        #region Private Methods
        private ConfigureEmailDto ConfigureEmail(string userName, string fullName, int userId)
        {
            var claimsdata = new[]
            {
                new Claim("UserId", userId.ToString()),
            };
            var token = GenerateToken(claimsdata, Convert.ToInt32(_configuration["ForgotPassword:MinuteExpireTolken"]));

            var parameter = _parameterRepository.GetAllParameter(0);
            var template = parameter.First(auth => auth.Name.Equals("BodyForgotFassword")).Value
                .Replace("[Name]", fullName)
                .Replace("[Link]", _configuration["ForgotPassword:UrlRecover"] + token);

            return _configureEmailRule.GetConfigureEmail("Affair", new[] { userName }, template);
        }

        private string GenerateToken(Claim[] claimsdata, int expiredMinutes)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var expiration = DateTime.UtcNow;

            var key = Encoding.ASCII.GetBytes(_configuration["ApiAuth:SecretKey"]);

            var token = new JwtSecurityToken(null, null,
                claimsdata, expiration,
                expiration.AddMinutes(expiredMinutes),
                new SigningCredentials(new SymmetricSecurityKey(key)
                    , SecurityAlgorithms.HmacSha256Signature));
            return tokenHandler.WriteToken(token);
        }

        private ClaimsPrincipal ReadToken(string token)
        {
            try
            {
                var secret = _configuration["ApiAuth:SecretKey"];
                var key = Encoding.ASCII.GetBytes(secret);
                var handler = new JwtSecurityTokenHandler();
                var tokenSecure = handler.ReadToken(token) as SecurityToken;
                var validations = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
                var claims = handler.ValidateToken(token, validations, out tokenSecure);
                return claims;
            }
            catch (SecurityTokenExpiredException)
            {
                Message = _message.Error(Resource.LinkExpired);
            }
            catch (Exception ex)
            {
                Message = _message.Exception(ex.Message);
            }
            return null;
        }
        #endregion
    }
}
