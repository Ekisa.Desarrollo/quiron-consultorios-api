﻿namespace Ekisa.Quiron.Rule
{
    using System.Threading.Tasks;
    using Dto;
    public interface IAuthorizationRule
    {
        MessageDto Message { get; }
        AuthorizationDto Authenticate(LoginDto dto);
        Task<bool> SendForgotPassword(string userName);
        void ForgotPassword(ForgotPasswordDto dto);
    }
}
