﻿namespace Ekisa.Quiron.Rule
{
    using Dto;
    using System.Collections.Generic;
    public interface IUserRule
    {
        MessageDto Message { get; }
        UserDto Create(UserDto dto);
        UserDto Update(UserDto dto);
        bool Delete(int id);
        UserDto GetUserByUserName(string userName);
        List<UserDto> GetAll();
    }
}
