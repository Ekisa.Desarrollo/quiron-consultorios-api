﻿namespace Ekisa.Quiron.Rule
{
    using Dto;
    using Resource;

    public class Message : IMessage
    {
        public MessageDto Exception(string message)
        {
            return new MessageDto { Type = TypeMessage.Exception, Flag = false, Message = message };
        }

        public MessageDto Exception()
        {
            return new MessageDto { Type = TypeMessage.Exception, Flag = false, Message = Resource.Exception };
        }

        public MessageDto Error(string message)
        {
            return new MessageDto { Type = TypeMessage.Error, Flag = false, Message = message };
        }

        public MessageDto Warning(string message)
        {
            return new MessageDto { Type = TypeMessage.Warning, Flag = false, Message = message };
        }

        public MessageDto Success()
        {
            return new MessageDto { Type = TypeMessage.Success, Flag = true, Message = Resource.Ok };
        }

        public MessageDto Success(string message)
        {
            return new MessageDto { Type = TypeMessage.Success, Flag = true, Message = message };
        }
    }
}
