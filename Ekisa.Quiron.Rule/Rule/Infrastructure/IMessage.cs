﻿namespace Ekisa.Quiron.Rule
{
    using Dto;

    public interface IMessage
    {
        MessageDto Exception(string message);
        MessageDto Exception();
        MessageDto Error(string message);
        MessageDto Warning(string message);
        MessageDto Success();
        MessageDto Success(string message);
    }
}
