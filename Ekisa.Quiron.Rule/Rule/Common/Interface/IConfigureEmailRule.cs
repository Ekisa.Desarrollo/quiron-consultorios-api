﻿namespace Ekisa.Quiron.Rule
{
    using Dto;
    using System.Threading.Tasks;
    public interface IConfigureEmailRule
    {
        Task<bool> SendEmailAsync(ConfigureEmailDto dto);
        ConfigureEmailDto GetConfigureEmail(string affair, string[] emails, string template);
    }
}
