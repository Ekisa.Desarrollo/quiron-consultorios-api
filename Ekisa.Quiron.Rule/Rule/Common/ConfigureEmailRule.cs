﻿namespace Ekisa.Quiron.Rule
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Common;
    using Dto;
    public class ConfigureEmailRule : IConfigureEmailRule
    {
        #region Private Attributes
        private readonly IEmail _email;
        private readonly IParameterRepository _parameterRepository;
        #endregion

        #region Constructor

        public ConfigureEmailRule(IEmail email,
            IParameterRepository parameterRepository)
        {
            _email = email;
            _parameterRepository = parameterRepository;
        }
        #endregion

        #region Public Methods      

        public async Task<bool> SendEmailAsync(ConfigureEmailDto dto)
        {
            return await _email.SendEmail(dto);
        }

        public ConfigureEmailDto GetConfigureEmail(string affair, string[] emails, string template)
        {
            var parameter = _parameterRepository.GetAllParameter(0);

            var configureEmail = new ConfigureEmailDto
            {
                SenderEmail = new SenderEmailDto
                {
                    Password = parameter.First(trans => trans.Name.Equals("PasswordSenderEmail")).Value,
                    Email = parameter.First(trans => trans.Name.Equals("SenderEmail")).Value,
                    ServerSmtp = parameter.First(trans => trans.Name.Equals("ServerSmtpSenderEmail")).Value,
                    EnableSsl = Convert.ToBoolean(Convert.ToInt32(parameter.First(trans => trans.Name.Equals("EnableSslSenderEmail")).Value)),
                    Port = Convert.ToInt32(parameter.First(trans => trans.Name.Equals("PortSenderEmial")).Value)
                },
                ReceiverEmail = new ReceiverEmailDto
                {
                    Emails = emails,
                    SendAttached = false,
                    Affair = affair,
                    BodyHtml = template,
                }
            };
            return configureEmail;
        }
        #endregion
    }
}
